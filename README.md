# KZMod Noclip

Simple noclip sourcemod plugin for kzmod (that invalidates your timer if you&#39;re noclipping).

Config gets auto generated in kz/cfg/sourcemod/plugins.kzmod-noclip.cfg.

Commands:  
// Normal noclip speed without +walk or +duck  
// -  
// Default: "1.0"  
sm_noclipspeed_duck "1.0"  
  
// Normal noclip speed without +walk or +duck  
// -  
// Default: "3.0"  
sm_noclipspeed_run "3.0"  
  
// Normal noclip speed without +walk or +duck  
// -  
// Default: "2.0"  
sm_noclipspeed_walk "2.0"  
