
#define DEBUG

#define PLUGIN_NAME				"KZMod Noclip"
#define PLUGIN_AUTHOR			"GameChaos"
#define PLUGIN_DESCRIPTION		"+noclip console command for Kreedz Climbing."
#define PLUGIN_VERSION			"1.02"
#define PLUGIN_URL				""

#define NOCLIP_SPEED			"3.0"
#define NOCLIP_SPEED_WALK		"2.0"
#define NOCLIP_SPEED_DUCK		"1.0"

#include <sourcemod>
#include <sdktools>
#include <kreedzclimbing>
#include <gamechaos/stocks>

#pragma semicolon 1

#pragma newdecls required

enum
{
	Noclip_Run,
	Noclip_Walk,
	Noclip_Duck
}

bool g_bNoclip[MAXPLAYERS + 1];
bool g_bInvalidRun[MAXPLAYERS + 1];
bool g_bLateLoad;

char g_szNoclipspeeds[3][8];

ConVar g_cvSvNoclipspeed;
ConVar g_cvSmNoclipspeeds[3];

public Plugin myinfo =
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
};

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	g_bLateLoad = late;
}

public void OnPluginStart()
{
	RegConsoleCmd("+noclip", Command_StartNoclip);
	RegConsoleCmd("-noclip", Command_EndNoclip);
	
	g_cvSmNoclipspeeds[Noclip_Run] = CreateConVar("sm_noclipspeed_run", NOCLIP_SPEED, "Normal noclip speed without +walk or +duck", FCVAR_NONE);
	g_cvSmNoclipspeeds[Noclip_Walk] = CreateConVar("sm_noclipspeed_walk", NOCLIP_SPEED_WALK, "Noclip speed with +walk", FCVAR_NONE);
	g_cvSmNoclipspeeds[Noclip_Duck] = CreateConVar("sm_noclipspeed_duck", NOCLIP_SPEED_DUCK, "Noclip speed with +duck", FCVAR_NONE);
	
	for (int i; i < 3; i++)
	{
		g_cvSmNoclipspeeds[i].AddChangeHook(OnConVarChanged);
		g_cvSmNoclipspeeds[i].GetString(g_szNoclipspeeds[i], sizeof(g_szNoclipspeeds[]));
	}
	
	AutoExecConfig();
	
	g_cvSvNoclipspeed = FindConVar("sv_noclipspeed");
	g_cvSvNoclipspeed.Flags = g_cvSvNoclipspeed.Flags & ~FCVAR_NOTIFY;
	
	if (g_bLateLoad)
	{
		for (int client = 1; client <= MaxClients; client++)
		{
			if (IsValidClient(client) && !IsFakeClient(client))
			{
				OnClientConnected(client);
			}
		}
	}
}

public void OnClientConnected(int client)
{
	g_bNoclip[client] = false;
	g_bInvalidRun[client] = false;
}

public void OnConVarChanged(ConVar convar, const char[] oldValue, const char[] newValue)
{
	for (int i; i < 3; i++)
	{
		if (convar == g_cvSmNoclipspeeds[i])
		{
			FormatEx(g_szNoclipspeeds[i], sizeof(g_szNoclipspeeds[]), newValue);
			return;
		}
	}
}

public Action Command_StartNoclip(int client, int args)
{
	if (!IsValidClient(client) || !IsPlayerAlive(client))
	{
		return Plugin_Handled;
	}
	
	SetEntityMoveType(client, MOVETYPE_NOCLIP);
	g_bNoclip[client] = true;
	return Plugin_Handled;
}

public Action Command_EndNoclip(int client, int args)
{
	if (!IsValidClient(client) || !IsPlayerAlive(client))
	{
		return Plugin_Handled;
	}
	
	g_bInvalidRun[client] = true;
	
	SetEntityMoveType(client, MOVETYPE_WALK);
	g_bNoclip[client] = false;
	return Plugin_Handled;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if (!IsValidClient(client) || !IsPlayerAlive(client) || IsFakeClient(client))
	{
		return Plugin_Continue;
	}
	
	if (g_bNoclip[client] || GetEntityMoveType(client) == MOVETYPE_NOCLIP)
	{
		g_bInvalidRun[client] = true;
		
		InvalidateTimer(client);
		
		if (buttons & IN_DUCK)
		{
			g_cvSvNoclipspeed.SetString(g_szNoclipspeeds[Noclip_Duck]);
			g_cvSvNoclipspeed.ReplicateToClient(client, g_szNoclipspeeds[Noclip_Duck]);
		}
		else if (buttons & IN_WALK)
		{
			g_cvSvNoclipspeed.SetString(g_szNoclipspeeds[Noclip_Walk]);
			g_cvSvNoclipspeed.ReplicateToClient(client, g_szNoclipspeeds[Noclip_Walk]);
		}
		else
		{
			g_cvSvNoclipspeed.SetString(g_szNoclipspeeds[Noclip_Run]);
			g_cvSvNoclipspeed.ReplicateToClient(client, g_szNoclipspeeds[Noclip_Run]);
		}
	}
	else if (g_bInvalidRun[client])
	{
		float fVelocity[3];
		GetEntPropVector(client, Prop_Data, "m_vecVelocity", fVelocity);
		
		if (InvalidateTimer(client))
		{
			PrintToConsole(client, "[kzmod-replays] Your timer was invalidated, because you noclipped");
		}
		
		if (GetVectorLength(fVelocity) < 250.0)
		{
			g_bInvalidRun[client] = false;
		}
	}
	return Plugin_Continue;
}